import flask
import socket
import collections

TITLE = 'boomlin.de'
DOMAIN = 'boomlin.de'
PORT = 70

TYPES = {
    '0': '(TXT)',
    '1': '(DIR)',
    'i': ' ',
    's': '(SND)',
    'g': '(GIF)',
    'I': '(PIC)',
    '9': '(BIN)',
    '5': '(ARC)',
}

MIME = {
    '0': 'text/plain',
    's': 'audio/unknown',
    'I': 'image/unknown',
    'g': 'image/gif',
    '9': 'application/octet-stream',
}

def read(server, path='', port=70):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((server, port))
    f = s.makefile()
    f.write(path + '\r\n')
    f.flush()
    return f

def gophersplit(server, path='', port=70):
    Row = collections.namedtuple('Row', ('item', 'text', 'selector', 'domain', 'port'))
    data = read(server, path, port).read()
    data = [row.split('\t') for row in data.split('\r\n') if '\t' in row]
    for i in range(len(data)):
        data[i] = [data[i][0][0], data[i][0][1:]] + data[i][1:]
        data[i] = Row(*data[i])
    return data


app = flask.Flask(__name__)

@app.template_filter('get_type')
def get_type(s):
    return TYPES.get(s, '(%s)' % s)

@app.route('/')
def main():
    data = gophersplit(DOMAIN, '', PORT)
    return flask.render_template('menu.html', rows=data, title=TITLE)

@app.route('/gopher/<item_type>/<path:selector>')
def gopher(item_type, selector=''):
    if item_type == '1':
        return flask.render_template('menu.html', rows=gophersplit(DOMAIN, selector, PORT), title=TITLE)
    else:
        mime = MIME.get(item_type, 'application/octet-stream')
        f = read(DOMAIN, selector, PORT)
        def generate():
            while True:
                data = f.read(8192)
                if not data:
                    break
                yield data
        return flask.Response(generate(), mimetype=mime)


if __name__ == '__main__':
    app.run()
